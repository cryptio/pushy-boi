# SENSify
SENSify is an Arduino project that uses a combination of NodeMCU and a little intuition to build a DIY home security device. Upon pressure plate activation/deactivation, a notification will be sent to your device to alert you.

## How it works
In order to understand how SENSify works, you first need a basic understanding of two conditions that a circuit can be in (we will not go over short circuits). An electrical circuit can either be in the "open" state, which means that current is unable to flow through the circuit or "closed", where the circuit is complete and the flow of the current is uninterrupted, thus reaching its destination.

Aluminum is a fairly good conductor for electricity. If we have a wire connected to one of our ground pins on the NodeMCU, and the other used for providing power (i.e 5V), we can take two pieces of aluminum foil and connect a wire to each one. When the pieces of aluminum are not touching each other, the circuit will be open because the flow of the current is interrupted. When both sheets of aluminum touch, the current is able to loop back around to the NodeMCU device, and the circuit becomes closed.

Now we know that a circuit can either be opened or closed by separating or touching the two pieces of aluminium, one can get a better sense of how SENSify works (no pun intended). If these two pieces of aluminum foil remain apart until the pressure of the foot touches them together, one can build a pressure plate and detect when it is being activated or not.

## Usage
In order to use this project, you must get a [PushBullet](https://www.pushbullet.com/) account. Afterwards, retrieve your PushBullet API key, setup a device for it and get the device ID, then put these in sensify.ino.

PushBullet allows you to programatically send notifications to a device. In my case, I hooked my cellular device up to it so I would get a notification on it. This worked out well as it is easy to test, and unlike SMS, PushBullet can be used for free as long as you have a working Internet connection.

## Building a pressure plate
Various pressure plate designs can be used, such as [this](https://www.youtube.com/watch?v=c6RXu0pYUVI) which uses cardboard. The approach that I took to this was using wooden plates that had a lot of flex to them, which provided more stability. A foam cutout was also put in the middle that added space around the perimeter of the board. This way, the two pieces of aluminium on the boards remain parallel until force is applied, at which point the circuit connects and closes.

![Pressure plate schematics](docs/plates.png)

## Wiring up your NodeMCU
As you can see in the diagram below, a breadboard is used. The 3V3 pin and ground wires are plugged into the beadboard, so we can use those rails to ground or power other things. The resistors shown in the diagram are 220Ω. A ground and power jumper cable is seen going out of the image, which are the two wires which connect to each sheet of your conductive metal respectively.

![NodeMCU wiring](docs/circuit.png)


