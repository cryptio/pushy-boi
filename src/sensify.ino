#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

const char *PB_API_KEY = "";  // Pushbullet API key
const char *PB_FINGERPRINT = "BB:FC:9F:1B:C1:3C:D9:96:F2:68:A2:E3:41:29:D1:47:8F:B9:33:BE"; // SHA1 certificate fingerprint
const char *PB_DEVICE_ID = "";  // Pushbullet device ID that the message gets sent to

const char *NETWORK_SSID = "";
const char *NETWORK_PASSWORD = ""; 

const int PRESSURE_PLATE_PIN = D1;
const int OUTPUT_PIN = D2;
const int BAUDRATE = 115200;
bool VAL = HIGH;  // Since a value of LOW means that there is input, we set this variable to the opposite initially

/**
 * Send a PushBullet notification to a device
 * @param device_id The destination device ID
 * @param title The title of the notification
 * @param message The notification message body
 */
void send_pushbullet_notification(const String &device_id, const String &title, const String &message);

void setup() {
  // initialize the pins
  pinMode(PRESSURE_PLATE_PIN, INPUT);
  pinMode(OUTPUT_PIN, OUTPUT);
  
  Serial.begin(BAUDRATE);

  Serial.print("Connecting to network " + String(NETWORK_SSID));
  WiFi.begin(NETWORK_SSID, NETWORK_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(500);
  }
  Serial.println();
  Serial.println("Succesfully connected to network");
  // Begin network information
  Serial.print("Local IP:");
  Serial.println(WiFi.localIP());
  Serial.print("MAC Address: ");
  // read the MAC address into an array of 6 integers, where each element is 1 byte
  uint8_t mac_address[6];
  WiFi.macAddress(mac_address);
  for (int i = 0; i < 6; ++i) {
    Serial.print(mac_address[i], HEX);
    if (i != 5) {
      Serial.print(':');    
    }
  }
  Serial.println();
  Serial.print("Subnet mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.printf("Gateway IP: %s\n", WiFi.gatewayIP().toString().c_str());
  Serial.print("Primary DNS IP: ");
  WiFi.dnsIP().printTo(Serial);
  Serial.println();
  Serial.print("Secondary DNS IP: ");
  WiFi.dnsIP(1).printTo(Serial);
  Serial.println();
  Serial.printf("Hostname: %s\n", WiFi.hostname().c_str());
  // End network information
}

// Send a PushBullet notification to a device
void send_pushbullet_notification(const String &device_id, const String &title, const String &message) {  
  WiFiClientSecure client;
  const char *pb_server = "api.pushbullet.com";
  const int https_port = 443;
  Serial.println("Connecting to server " + String(pb_server));
  if (client.connect(pb_server, https_port)) {
    Serial.println("Successfully connected to server");
  } else {
    Serial.println("Error connecting to server");
  }

  if (client.verify(PB_FINGERPRINT, pb_server)) {
    Serial.println("Successfully verified signature");
  } else {
    Serial.println("Failed to verify signature");
  }

  const String pb_pushes_endpoint = "/v2/pushes";
  const String pb_request_body = "{\"type\": \"note\", \"title\": \"" + title + "\", \"body\": \"" + message + "\"}\r\n";
  const String pb_http_header = 
  "POST " + pb_pushes_endpoint + " HTTP/1.1\r\n" + 
  "Host: " + String(pb_server) + "\r\n" +
  "Authorization: Bearer " + PB_API_KEY + "\r\n" +
  "Content-Type: application/json\r\n" + 
  "Connection: close\r\n" + 
  "Content-Length: " +  String(pb_request_body.length())
  + "\r\n\r\n";
  Serial.println("Sent header : " + pb_http_header);
  client.print(pb_http_header);
  client.print(pb_request_body);

  Serial.println("Request sent");

// This causes it to block
//  while (client.connected()) {
//    String line = client.readStringUntil('\n');
//    if (line == "\r") {
//      Serial.println("Headers received");
//      break;
//    }
//  }
//  String line = client.readStringUntil('\n');
//  Serial.println("Response is " + line);

}

void loop() {
  const bool current_value = digitalRead(PRESSURE_PLATE_PIN);
  if (current_value != VAL) { // Only do something when there is a change in value
    VAL = current_value;
    // Since we are using a pull-up resistor, it will read a low state when the pressure plate is activated
    if (current_value == LOW) {
      digitalWrite(OUTPUT_PIN, HIGH);  // turn on the LED
      send_pushbullet_notification(PB_DEVICE_ID, "Pressure plate pressed", "The pressure plate was just activated. When it is deactived you will receive another notification.");
    } else {
      digitalWrite(OUTPUT_PIN, LOW);
      send_pushbullet_notification(PB_DEVICE_ID, "Pressure plate released", "The pressure plate was just deactivated. ");
    }
  }
}